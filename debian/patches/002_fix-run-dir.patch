Author: Francois Marier <francois@debian.org>
Bug: https://github.com/mrash/fwknop/issues/293
Forwarded: https://github.com/mrash/fwknop/pull/300
Description: Use the correct configure variable for /run
Last-Update: 2019-07-06

--- a/doc/fwknopd.man.asciidoc
+++ b/doc/fwknopd.man.asciidoc
@@ -77,7 +77,7 @@ COMMAND-LINE OPTIONS
 *-d, --digest-file*='<digest-file>'::
     Specify the location of the 'digest.cache' file. If this option is
     not given, 'fwknopd' will use the compile-time default location (typically
-    '@localstatedir@/fwknop/digest.cache').
+    '@runstatedir@/fwknop/digest.cache').
 
 *-D, --dump-config*::
     Dump the configuration values that *fwknopd* derives from the
@@ -126,7 +126,7 @@ COMMAND-LINE OPTIONS
     start processing network traffic.
 
 *--exit-parse-digest-cache*::
-    Parse the digest cache file '@localstatedir@/fwknop/digest.cache'
+    Parse the digest cache file '@runstatedir@/fwknop/digest.cache'
     and exit. This validates the structure of the digest cache file without
     having to start processing network traffic. Note that the standard
     configuration files are also parsed in this mode.
@@ -168,7 +168,7 @@ COMMAND-LINE OPTIONS
 *-p, --pid-file*='<pid-file>'::
     Specify the location of the 'fwknopd.pid' file. If this option is
     not given, 'fwknopd' will use the compile-time default location (typically
-    '@localstatedir@/fwknop/fwknopd.pid').
+    '@runstatedir@/fwknop/fwknopd.pid').
 
 *-P, --pcap-filter*='<filter>'::
     Specify a Berkeley packet filter statement on the *fwknopd* command
@@ -202,11 +202,11 @@ COMMAND-LINE OPTIONS
 *--rotate-digest-cache*::
     Rotate the digest cache file by renaming it to ``<name>-old'', and
     starting a new one. The digest cache file is typically found in
-    '@localstatedir@/fwknop/digest.cache'.
+    '@runstatedir@/fwknop/digest.cache'.
 
 *-r, --run-dir*='<path>'::
     Specify the directory where *fwknopd* writes run time state files. The
-    default is '@localstatedir@'.
+    default is '@runstatedir@'.
 
 *-S, --status*::
     Display the status of any *fwknopd* processes that may or not be
@@ -467,7 +467,7 @@ the '@sysconfdir@/fwknop/fwknopd.conf' file for additional details.
 
 *FWKNOP_RUN_DIR* '<path>'::
     Specify the directory where *fwknopd* writes run time state files. The
-    default is '@localstatedir@'.
+    default is '@runstatedir@'.
 
 ACCESS.CONF VARIABLES
 ~~~~~~~~~~~~~~~~~~~~~
--- a/server/Makefile.am
+++ b/server/Makefile.am
@@ -25,7 +25,7 @@ fwknopd_LDADD     = $(top_builddir)/lib/libfko.la $(top_builddir)/common/libfko_
 if WANT_C_UNIT_TESTS
     noinst_PROGRAMS         = fwknopd_utests
     fwknopd_utests_SOURCES  = fwknopd_utests.c $(BASE_SOURCE_FILES)
-    fwknopd_utests_CPPFLAGS = -I $(top_builddir)/lib -I $(top_builddir)/common $(GPGME_CFLAGS) -DSYSCONFDIR=\"$(sysconfdir)\" -DSYSRUNDIR=\"$(localstatedir)\"
+    fwknopd_utests_CPPFLAGS = -I $(top_builddir)/lib -I $(top_builddir)/common $(GPGME_CFLAGS) -DSYSCONFDIR=\"$(sysconfdir)\" -DSYSRUNDIR=\"$(runstatedir)\"
     fwknopd_utests_LDADD    = $(top_builddir)/lib/libfko.la $(top_builddir)/common/libfko_util.a
     fwknopd_utests_LDFLAGS  = -lcunit $(GPGME_LIBS)
 
@@ -56,7 +56,7 @@ else
 endif
 endif
 
-fwknopd_CPPFLAGS  = -I $(top_srcdir)/lib -I $(top_srcdir)/common -DSYSCONFDIR=\"$(sysconfdir)\" -DSYSRUNDIR=\"$(localstatedir)\"
+fwknopd_CPPFLAGS  = -I $(top_srcdir)/lib -I $(top_srcdir)/common -DSYSCONFDIR=\"$(sysconfdir)\" -DSYSRUNDIR=\"$(runstatedir)\"
 
 fwknopddir        = @sysconfdir@/fwknop
 
--- a/server/fwknopd.8.in
+++ b/server/fwknopd.8.in
@@ -103,7 +103,7 @@ Specify the location of the
 file\&. If this option is not given,
 \fIfwknopd\fR
 will use the compile\-time default location (typically
-\fI@localstatedir@/fwknop/digest\&.cache\fR)\&.
+\fI@runstatedir@/fwknop/digest\&.cache\fR)\&.
 .RE
 .PP
 \fB\-D, \-\-dump\-config\fR
@@ -181,7 +181,7 @@ Parse config files (\fI@sysconfdir@/fwknop/fwknopd\&.conf\fR, and
 \fB\-\-exit\-parse\-digest\-cache\fR
 .RS 4
 Parse the digest cache file
-\fI@localstatedir@/fwknop/digest\&.cache\fR
+\fI@runstatedir@/fwknop/digest\&.cache\fR
 and exit\&. This validates the structure of the digest cache file without having to start processing network traffic\&. Note that the standard configuration files are also parsed in this mode\&.
 .RE
 .PP
@@ -239,7 +239,7 @@ Specify the location of the
 file\&. If this option is not given,
 \fIfwknopd\fR
 will use the compile\-time default location (typically
-\fI@localstatedir@/fwknop/fwknopd\&.pid\fR)\&.
+\fI@runstatedir@/fwknop/fwknopd\&.pid\fR)\&.
 .RE
 .PP
 \fB\-P, \-\-pcap\-filter\fR=\fI<filter>\fR
@@ -287,7 +287,7 @@ files\&. This will also force a flush of the current \(lqFWKNOP\(rq iptables cha
 \fB\-\-rotate\-digest\-cache\fR
 .RS 4
 Rotate the digest cache file by renaming it to \(lq<name>\-old\(rq, and starting a new one\&. The digest cache file is typically found in
-\fI@localstatedir@/fwknop/digest\&.cache\fR\&.
+\fI@runstatedir@/fwknop/digest\&.cache\fR\&.
 .RE
 .PP
 \fB\-r, \-\-run\-dir\fR=\fI<path>\fR
@@ -295,7 +295,7 @@ Rotate the digest cache file by renaming it to \(lq<name>\-old\(rq, and starting
 Specify the directory where
 \fBfwknopd\fR
 writes run time state files\&. The default is
-\fI@localstatedir@\fR\&.
+\fI@runstatedir@\fR\&.
 .RE
 .PP
 \fB\-S, \-\-status\fR
@@ -612,7 +612,7 @@ will set the destination field on the firewall rule to the destination address s
 Specify the directory where
 \fBfwknopd\fR
 writes run time state files\&. The default is
-\fI@localstatedir@\fR\&.
+\fI@runstatedir@\fR\&.
 .RE
 .SS "ACCESS\&.CONF VARIABLES"
 .sp
